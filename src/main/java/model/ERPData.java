package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class represents an ERP data item arriving via JMS as XML message.
 *
 * @author julian.reichwald@dhbw-mannheim.de
 *
 */
@XmlRootElement
public class ERPData {

    private String OrderNumber;

    private int CustomerNumber;

    private int MaterialNumber;

    private Date TimeStamp;
    
    //OPC data related to this ERP one
    private ObservableList<OPCDataItem> dataOPC = FXCollections.observableArrayList();
    

    public ERPData(String OrderNumber, int CustomerNumber, int MaterialNumber, Date TimeStamp) {
        this.OrderNumber = OrderNumber;
        this.CustomerNumber = CustomerNumber;
        this.MaterialNumber = MaterialNumber;
        this.TimeStamp = TimeStamp;
    }

    public ERPData() {

    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public int getCustomerNumber() {
        return CustomerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        CustomerNumber = customerNumber;
    }

    public int getMaterialNumber() {
        return MaterialNumber;
    }

    public void setMaterialNumber(int materialNumber) {
        MaterialNumber = materialNumber;
    }

    public Date getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        TimeStamp = timeStamp;
    }

    
    public ObservableList<OPCDataItem> getDataOPC() {
        return dataOPC;
    }

    public void setDataOPC(ObservableList<OPCDataItem> dataOPC) {
        this.dataOPC = dataOPC;
    }

    public void addOPCItem(OPCDataItem opcObj) {
        dataOPC.add(opcObj);
    }
    
    

}
