TODOS para trabalho
==================

## 0. ALTERAR OS COMETÁRIOS PARA INGLÊS INCLUSIVE ESTE ARQUIVO 

## 1. Criar uma classe que representa - OK 

```
{ "em1":1503.345170530982, "em2":299.67462095849083, "a1":1.0825945436219175,
"a2":0.45468970832120537, "b2":136.25896599488397, "b1":1627.5132787972022,
"overallStatus":"NOK", "ts_start":1437256092444,"ts_stop":1437256103444}
```

## 2. Tarefa para individualização no tratamento das mensagens opc 
   2.1 desenhar e implementar uma máquina de estado
   2.2 representar os estados (das mensagens "opcs" )
   2.3 disparar gatilhos para cada etapa 

## 3. Criar tabelas para arquivamento e apresentação de cada erpitem 
   3.1 Apresentar uma tabela para os ERPs e outra para os eventos de cada um


# Observações

## 1. Lista de eventos:
```xml
<eventos>
    <itemName>Lichtschranke 1</itemName>
    <itemName>Lichtschranke 1</itemName>
    <itemName>Drilling Heat</itemName>
    <itemName>Drilling Heat</itemName>
    <itemName>Lichtschranke 2</itemName>
    <itemName>Lichtschranke 2</itemName>
    <itemName>Drilling Speed</itemName>
    <itemName>Drilling Heat</itemName>
    <itemName>Drilling Heat</itemName>
    <itemName>Drilling Speed</itemName>
    <itemName>Drilling Heat</itemName>
    <itemName>Drilling Station</itemName>
    <itemName>Milling Station</itemName>
    <itemName>Milling Speed</itemName>
    <itemName>Lichtschranke 4</itemName>
    <itemName>Milling Heat</itemName>
    <itemName>Milling Heat</itemName>
    <itemName>Lichtschranke 5</itemName>
    <itemName>Lichtschranke 5</itemName>
    <itemName>Milling Speed</itemName>
    <itemName>Milling Heat</itemName>
    <itemName>Milling Heat</itemName>
    <itemName>Milling Speed</itemName>
    <itemName>Milling Heat</itemName>
    <itemName>Milling Heat</itemName>
    <itemName>Milling Station</itemName>
    <itemName>Lichtschranke 3</itemName>
    <itemName>Lichtschranke 4</itemName>
    <itemName>Drilling Station</itemName>
    <itemName>Drilling Speed</itemName>
```